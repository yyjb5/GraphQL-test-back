# 说明
用json文件模拟数据库，提供GrapQL的用户查询添加接口测试，为初学注册登录准备
# 用法
``` shell
yarn install
yarn dev
```
# 接口地址
http://localhost:4000/
# 接口示例
## GraphQl
1. 查询
``` GrappQL
query {
    user(name: "user0", password: "5555"){
        id
        name
        type
    }
}
```
2. 添加
``` GrappQL
mutation {
    user(id: 0,name: "user0", password: "5555", type: 0){
        id
        name
        type
    }
}
```
# 转换为js文件
``` shell
yarn tsc
```
然后 在dist目录新建data目录