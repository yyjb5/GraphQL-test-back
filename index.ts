import { ApolloServer } from 'apollo-server';
import type { ServerInfo } from 'apollo-server';
import typeDefs from './src/model/shema/shema';
import resolvers from './src/model/resolvers/resolver';
import path from 'path';
import { dbFs } from './src/db';

const dbPath = path.resolve(__dirname, 'data/db.json');

const theDbFs = dbFs.getInstance(dbPath);


const server = new ApolloServer({
    typeDefs,
    resolvers,
    csrfPrevention: true,
    context: {
        theDbFs
    }
});

server.listen().then(({ url }: ServerInfo) => {
    console.log(`🚀  Server ready at ${url}`);
});