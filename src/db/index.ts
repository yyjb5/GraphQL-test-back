import fs from 'fs/promises';

type user = {
    id: number,
    name: string,
    password: string,
    type: number
}

export class dbFs {
    private dbPath = '';
    private static dbFsI: dbFs;

    public static getInstance(dbPath: string): dbFs {
        if (!this.dbFsI) {
            this.dbFsI = new dbFs(dbPath);
        }
        return this.dbFsI;
    }

    private constructor(dbPath: string) {
        this.dbPath = dbPath;
    }

    public async findOne(name: string, password: string) {
        const getBuffe = await fs.readFile(this.dbPath, { encoding: 'utf8', flag: 'a+' });
        const dbObject: user[] = JSON.parse(getBuffe);
        return dbObject.find((item) => (item.name === name && item.password === password));
    }

    public async appendOne(item: user) {
        try {
            let getBuffe = await fs.readFile(this.dbPath, { encoding: 'utf8', flag: 'a+' });
            if (!getBuffe) {
                getBuffe = '[]';
            }
            const dbObject: user[] = JSON.parse(getBuffe);
            dbObject.push(item);
            const getJson = JSON.stringify(dbObject, null, '\t');
            await fs.writeFile(this.dbPath, getJson);
        } catch (error) {
            throw new Error(`错误${error}`);
        }

    }
}

