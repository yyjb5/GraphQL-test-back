import type { dbFs } from '../../db';
const resolvers = {
    Query: {
        user: async (_parent: unknown, args: { name: string, password: string }, context: { theDbFs: dbFs }) => {
            const getResult = context.theDbFs.findOne(args.name, args.password);
            return getResult;
        },
    },
    Mutation: {
        user: async (
            _parent: unknown,
            args: {
                id: number,
                name: string,
                password: string,
                type: number
            },
            context: { theDbFs: dbFs }
        ) => {
            try {
                context.theDbFs.appendOne(args);
            } catch (error) {
                return error;
            }

            return {
                id: 0,
                name: 'user0',
                password: '5555',
                type: 0
            };
        }
    }
};
export default resolvers;
