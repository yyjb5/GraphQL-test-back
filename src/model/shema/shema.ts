import { gql } from 'apollo-server';
const typeDefs = gql`
  type User {
    id: ID!
    name: String!
    password: String!
    type: Int!
  }
  type Query {
    user(name: String!, password: String!): User!
  }
  type Mutation {
    user(id: Int, name: String!, password: String!, type: Int!) : User!
  }
`;
export default typeDefs;
